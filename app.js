(function() {
  //@ts-check
  "use strict";
  angular
    .module("logoApp", [
      "ui.bootstrap",
      "simpleFileReader",
      "jdFontselect",
      "firebase"
    ])
    .constant("jdFontselectConfig", {
      googleApiKey: "AIzaSyAmdSqOsXl_cBEhf5HmOWNbCRmTU20taVI"
    })
    .controller("MainController", MainController)
    .controller("PictoBoardController", PictoBoardController)
    .controller("FlashReadingController", FlashReadingController)
    .controller("PictoAdmin", PictoAdmin)
    .service("PictoService", PictoService)
    .factory("flashSessions", flashSessions);

  // this factory returns a synchronized array of flash sessions
  function flashSessions($firebaseArray) {
    // create a reference to the database location where we will store our data
    var ref = firebase.database().ref();
    // this uses AngularFire to create the synchronized array
    return $firebaseArray(ref);
  }

  function PictoService($http) {
    var vm = this;
    vm.baseApiUrl = "//picto-api.bmaron.net/";
    vm.pictoBaseUrl = vm.baseApiUrl + "static/";

    this.delete = function(pictoId) {
      return $http.delete(vm.baseApiUrl + "delete/" + pictoId);
    };

    this.rename = function(pictoId, newName) {
      return $http.post(vm.baseApiUrl + "rename/" + pictoId, {
        name: newName
      });
    };

    this.add = function(name, file) {
      var fd = new FormData();
      fd.append("picto", file);
      fd.append("name", name);

      return $http.post(vm.baseApiUrl + "add-picto/", fd, {
        transformRequest: angular.identity,
        headers: {
          "Content-Type": undefined
        }
      });
    };

    this.search = function(term) {
      return $http.get(vm.baseApiUrl + "term/" + term).then(function(response) {
        return response.data;
      });
    };

    this.getPictoOrPlaceholder = function(picto) {
      if (picto && picto.filename) {
        return vm.pictoBaseUrl + picto.filename;
      }
      return "placeholder.png";
    };
  }

  function MainController($firebaseAuth) {
    var vm = this;

    vm.init = function() {
      vm.visibleScreen = "flash";
      vm.auth = $firebaseAuth();
      vm.auth.$onAuthStateChanged(function(firebaseUser) {
        vm.firebaseUser = firebaseUser;
        if (firebaseUser) {
          //console.log("Signed in as:", firebaseUser.uid);
        }
      });
    };

    vm.signIn = function() {
      vm.auth.$signInWithPopup("google");
    };

    vm.signOut = function() {
      vm.auth.$signOut();
    };

    vm.init();
  }

  function PictoAdmin(fileReader, $scope, PictoService) {
    var vm = this;

    vm.init = function() {
      vm.searchResult = [];
      vm.searchTerm = "";
      vm.addFormVisible = false;
    };

    vm.search = function() {
      return PictoService.search(vm.searchTerm).then(function(result) {
        vm.searchResult = result;
      });
    };

    vm.getPictoOrPlaceholder = function(picto) {
      return PictoService.getPictoOrPlaceholder(picto);
    };

    vm.getFile = function() {
      fileReader.readAsDataUrl(vm.file, $scope).then(function(result) {
        vm.imageSrc = result;
      });
    };

    vm.add = function() {
      PictoService.add(vm.name, vm.file).then(function(r) {
        vm.addFormVisible = false;
      });
    };

    vm.delete = function(picto, index) {
      PictoService.delete(picto.id).then(function() {
        vm.searchResult.splice(index, 1);
      });
    };

    vm.edit = function(picto, index) {
      PictoService.rename(picto.id, picto.name).then(function() {
        picto.edit = false;
      });
    };

    vm.init();
  }

  function FlashReadingController(
    $timeout,
    jdFontselectFonts,
    flashSessions,
    PictoService
  ) {
    var vm = this;

    vm.init = function() {
      vm.flashModes = {
        pause: "Rafale",
        words: "Proposition Mots",
        pictos: "Proposition Pictos"
      };

      vm.wordIndex = null;
      vm.flashMode = "pause";
      vm.sentenses = [];
      vm.intervalofDisplay = 2000;
      vm.intervalBetween = 500;
      vm.fontSize = 50;
      vm.isStarted = false;
      vm.flashSessions = flashSessions;
      vm.folders = "";
      // vm.sessionsByFolder = [];
      vm.parentFolders = [];
      // vm.flashSessions.$watch(vm.getSessionsByFolder);

      vm.initJdFont();
    };

    vm.initJdFont = function() {
      var font = {
        name: "OpenDyslexic",
        key: "opendyslexic",
        category: "sansserif",
        stack: '"opendyslexic", sans-serif',
        popularity: 100,
        lastModified: "2014-01-28",
        subsets: ["latin"]
      };
      jdFontselectFonts.registerProvider("pictogo", function() {});
      jdFontselectFonts.add(font, "pictogo");
      jdFontselectFonts.getProviders()["pictogo"] = true;
    };

    vm.getPictoOrPlaceholder = function(picto) {
      return PictoService.getPictoOrPlaceholder(picto);
    };

    vm.addWord = function() {
      vm.sentenses.push({
        word: "",
        proposition_1: "",
        picto_1: "",
        proposition_2: "",
        picto_2: ""
      });
    };

    vm.removeWord = function(index) {
      vm.sentenses.splice(index, 1);
    };

    vm.getPicto = function(val) {
      return PictoService.search(val);
    };

    vm.addSession = function(name, folders, uid) {
      if (!name) {
        name = "Session " + Math.round(Math.random() * 100);
      }
      var foldersArray = folders.split("/").filter(function(i) {
        return i;
      });
      console.log(foldersArray);
      vm.flashSessions.$add({
        name: name,
        folders: foldersArray,
        mode: vm.flashMode,
        sentenses: vm.sentenses,
        intervalofDisplay: vm.intervalofDisplay,
        intervalBetween: vm.intervalBetween,
        fontSize: vm.fontSize,
        font: vm.fontChoice,
        uid: uid
      });
    };

    vm.setFolder = function(folder) {
      vm.parentFolders = folder;
    };

    vm.getSessionsByFolder = function() {
      var sessionsByFolder = vm.flashSessions.filter(function(session) {
        var loopf = session["folders"] || [];

        return (
          !vm.parentFolders.length ||
          vm.parentFolders.every(function(parent) {
            return loopf.includes(parent);
          })
        );
      });

      var sessionList = sessionsByFolder.filter(function(session) {
        var loopf = session["folders"] || [];

        return (
          (!vm.parentFolders.length && !loopf.length) ||
          loopf.length == vm.parentFolders.length
        );
      });

      var directoryList = sessionsByFolder
        .filter(function(session) {
          return (
            session["folders"] &&
            session["folders"].length == vm.parentFolders.length + 1
          );
        })
        .reduce(function(groups, item) {
          const folders = item["folders"] || [];
          const folderLevel = vm.parentFolders.length;
          const folder = folders[folderLevel] || "";
          groups[folder] = groups[folder] || [];
          groups[folder].push(item);
          return groups;
        }, {});

      return Object.keys(directoryList).concat(sessionList);
    };

    vm.loadSession = function(session) {
      vm.name = session.name;
      vm.folders = session.folders;
      vm.sentenses = session.sentenses;
      vm.flashMode = session.mode;
      vm.intervalofDisplay = session.intervalofDisplay;
      vm.intervalBetween = session.intervalBetween;
      vm.fontChoice = session.font;
      vm.fontSize = session.fontSize;
      vm.randomItemsArray = [];
    };

    vm.deleteSession = function(session) {
      vm.flashSessions.$remove(session);
    };

    vm.isSessionsLoading = function() {
      return !vm.flashSessions.$resolved;
    };

    vm.start = function() {
      vm.isStarted = true;
      vm.wordIndex = -1;
      vm.randomItemsArray = [];
      // Init the item array
      for (var i = 0; i < vm.sentenses.length; i++) {
        vm.randomItemsArray.unshift(i);
      }
      if (vm.isRandom) {
        vm.shuffleArray(vm.randomItemsArray);
      }

      if (vm.flashMode == "pause") {
        vm.nextWord();
      } else {
        vm.nextPropostion();
      }
    };

    vm.shuffleArray = function(array) {
      for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
      }
    };

    vm.showPropostion = function() {
      vm.isBetween = true;
    };

    vm.getNextIndex = function() {
      vm.wordIndex = vm.randomItemsArray.pop();
    };

    vm.getRandomArbitrary = function(min, max) {
      return Math.random() * (max - min) + min;
    };

    vm.nextPropostion = function(response) {
      vm.isBetween = false;
      vm.getNextIndex();
      if (vm.wordIndex < vm.sentenses.length) {
        $timeout(vm.showPropostion, vm.intervalofDisplay);
      } else {
        vm.finish();
      }
    };

    vm.nextWord = function() {
      if (!vm.isStarted) {
        // stopped
        return;
      }
      vm.isBetween = true;
      $timeout(vm.showWord, vm.intervalBetween);

      vm.getNextIndex();
      if (vm.wordIndex < vm.sentenses.length) {
        $timeout(vm.nextWord, vm.intervalofDisplay + vm.intervalBetween);
      } else {
        vm.finish();
      }
    };

    vm.showWord = function() {
      vm.isBetween = false;
    };

    vm.finish = function() {
      vm.isStarted = false;
    };

    vm.fullScreen = function() {
      var elem = document.getElementById("flash-content");

      if (
        !document.fullscreenElement && // alternative standard method
        !document.mozFullScreenElement &&
        !document.webkitFullscreenElement
      ) {
        // current working methods
        if (elem.requestFullscreen) {
          elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) {
          elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
          elem.webkitRequestFullscreen();
        }
      } else {
        if (document.cancelFullScreen) {
          document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
          document.webkitCancelFullScreen();
        }
      }
    };

    vm.getFirstWords = function(wordsToRead) {
      var words = wordsToRead || [];
      return words.slice(0, 6).map(function(el) {
        return el.word;
      });
    };

    vm.init();
  }

  function PictoBoardController(PictoService, fileReader, $scope) {
    var vm = this;

    vm.init = function() {
      vm.pictoList = [];
    };

    vm.onSelectPicto = function(index, $item) {
      if (vm.pictoList[index].title == "") {
        vm.pictoList[index].title = $item.name;
      }
    };

    vm.addPicto = function() {
      vm.pictoList.push({
        image: null,
        title: ""
      });
    };

    vm.getPictoOrPlaceholder = function(picto) {
      return PictoService.getPictoOrPlaceholder(picto);
    };

    vm.getPicto = function(val) {
      return PictoService.search(val);
    };

    vm.removePicto = function(index) {
      vm.pictoList.splice(index, 1);
    };

    vm.getFile = function() {
      fileReader.readAsDataUrl(vm.file, $scope).then(function(result) {
        vm.imageSrc = result;
      });
    };

    vm.init();
  }
})();
